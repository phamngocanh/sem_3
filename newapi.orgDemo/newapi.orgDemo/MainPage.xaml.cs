﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;
using newapi.orgDemo.Models;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace newapi.orgDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Articles = new ObservableCollection<Article>();
            GetAR("bitcoin", "2019-09-10", "publishedAt", "582b6408924c44e4be66bd79a318ddd0");
        }

        public ObservableCollection<Article> Articles { get; private set; }

        private async void GetAR(string q, string from, string SortBy, string ApiKey)
        {
            var url = string.Format("https://newsapi.org/v2/everything?q=fashion&from=2019-10-10&sortBy=publishedAt&apiKey=582b6408924c44e4be66bd79a318ddd0", q, from, SortBy, ApiKey);
            var news = await New.GetNew(url) as RootObject;
            news.articles.ForEach(n =>
            {
                Articles.Add(n);
            });
        }

        private void View_ItemClick(object sender, ItemClickEventArgs e)
        {
            Article ar = e.ClickedItem as Article;
            Frame.Navigate(typeof(ArticleView), ar);

        }
    }
}
