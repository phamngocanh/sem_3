﻿using System.Collections.Generic;
using ObservableCollection.Model;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Collections.ObjectModel;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ObservableCollection
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private List<Icon> Icons;
        private ObservableCollection<Contact> Contacts;
        public MainPage()
        {
            this.InitializeComponent();
            Icons = new List<Icon>();
            Icons.Add(new Icon { IconPath = "Assets/Images01/male-01.png" });
            Icons.Add(new Icon { IconPath = "Assets/Images01/male-02.png" });
            Icons.Add(new Icon { IconPath = "Assets/Images01/male-03.png" });
            Icons.Add(new Icon { IconPath = "Assets/Images01/female-01.png" });
            Icons.Add(new Icon { IconPath = "Assets/Images01/female-02.png" });
            Icons.Add(new Icon { IconPath = "Assets/Images01/female-03.png" });
            Contacts = new ObservableCollection<Contact>();
        }
        private void NewContactButton_Click(object sender, RoutedEventArgs e)
        {
            string avatar = ((Icon)AvartarComboBox.SelectedValue).IconPath;
            Contacts.Add(new Contact
            {
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                AvatarPath = avatar
            });
            FirstNameTextBox.Text = "";
            LastNameTextBox.Text = "";
            AvartarComboBox.SelectedIndex = -1;
            FirstNameTextBox.Focus(FocusState.Programmatic);

        }

        private void NewContactButton_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
